﻿
//============================================================
// Student Number : S10155754, S10204599
// Student Name : Jeow Teng Xiang Cody, Ng Lynn Shaan
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }

        public string Nationality { get; set; }

        public Visitor(string n, string p, string na) : base (n)
        {
            PassportNo = p;
            Nationality = na;
        }

        public override double CalculateSHNCharges(TravelEntry t)
        {
            int difference = (t.ShnEndDate - t.EntryDate).Days;
            if (difference == 0)
            {
                return 200 + 80;
            }

            else if (difference == 7)
            {
                return 200 + 80;
            }

            else
            {
                return 200 + 2000;
            }
        }

        public override string ToString()
        {
            return base.ToString() + string.Format("{0,-20}{1,-8}", PassportNo, Nationality);
        }
    }
}


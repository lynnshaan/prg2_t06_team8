﻿
//============================================================
// Student Number : S10155754
// Student Name : Jeow Teng Xiang Cody
// Module Group : T06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG_Assessment  
{
    class BusinessLocation
    {
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }

        public BusinessLocation()
        {

        }
        public BusinessLocation(string bn, string bc, int mc)
        {
            BusinessName = bn;
            BranchCode = bc;
            MaximumCapacity = mc;
            VisitorsNow = 0;
        }
        public bool IsFull()
        {
            if (VisitorsNow == MaximumCapacity)
            {
                return true;
            }
            else
            {
                return false;
            }
                
        }
        public override string ToString()
        {
            return string.Format("{0,-22}{1,-20}{2,-20}{3,-20}", BusinessName, BranchCode, MaximumCapacity, VisitorsNow);
        }

    }
}
